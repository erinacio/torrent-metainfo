package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/anacrolix/torrent/metainfo"
)

type fileInfoData struct {
	Length int64  `json:"length"`
	Path   string `json:"path"`
}

type infoData struct {
	Name    string         `json:"name,omitempty"`
	Length  int64          `json:"length,omitempty"`
	Private *bool          `json:"private,omitempty"`
	Source  string         `json:"source,omitempty"`
	Files   []fileInfoData `json:"files"`
}

type metaInfoData struct {
	AnnounceList [][]string `json:"announce_list,omitempty"`
	Nodes        []string   `json:"nodes,omitempty"`
	CreationDate int64      `json:"creation_date,omitempty"`
	Comment      string     `json:"comment,omitempty"`
	CreatedBy    string     `json:"created_by,omitempty"`
	Encoding     string     `json:"encoding,omitempty"`
	UrlList      []string   `json:"url_list,omitempty"`
	Info         infoData   `json:"info"`
}

func DumpJson(out io.Writer, metainfos []metaInfoData) {
	enc := json.NewEncoder(out)
	enc.SetIndent("", "  ")
	if len(metainfos) == 1 {
		_ = enc.Encode(metainfos[0])
	} else {
		if metainfos == nil {
			metainfos = []metaInfoData{}
		}
		_ = enc.Encode(metainfos)
	}
}

func DumpReadable(out io.Writer, metainfos []metaInfoData) {
	for _, meta := range metainfos {
		if len(metainfos) > 0 {
			fmt.Fprintf(out, "==========================================\n")
		}
		fmt.Fprintf(out, "Announces:\n")
		for _, announces := range meta.AnnounceList {
			for i, announce := range announces {
				if i == 0 {
					fmt.Fprintf(out, " - - %s\n", announce)
				} else {
					fmt.Fprintf(out, "   - %s\n", announce)
				}
			}
		}
		if len(meta.Nodes) > 0 {
			fmt.Fprintf(out, "Nodes:\n")
			for _, node := range meta.Nodes {
				fmt.Fprintf(out, " - %s\n", node)
			}
		}
		if meta.CreationDate != 0 {
			fmt.Fprintf(out, "CreationDate: %s\n",
				time.Unix(meta.CreationDate, 0).Local().Format(time.RFC1123))
		}
		if meta.Comment != "" {
			fmt.Fprintf(out, "Comment: %s\n", meta.Comment)
		}
		if meta.CreatedBy != "" {
			fmt.Fprintf(out, "CreatedBy: %s\n", meta.CreatedBy)
		}
		if meta.Encoding != "" {
			fmt.Fprintf(out, "Encoding: %s\n", meta.Encoding)
		}
		if len(meta.UrlList) > 0 {
			fmt.Fprintf(out, "UrlList:\n")
			for _, url := range meta.UrlList {
				fmt.Fprintf(out, " - %s\n", url)
			}
		}
		if meta.Info.Name != "" {
			fmt.Fprintf(out, "Info.Name: %s\n", meta.Info.Name)
		}
		if meta.Info.Length != 0 {
			fmt.Fprintf(out, "Info.Length: %d\n", meta.Info.Length)
		}
		if meta.Info.Source != "" {
			fmt.Fprintf(out, "Info.Source: %s\n", meta.Info.Source)
		}
		if len(meta.Info.Files) > 0 {
			fmt.Fprintf(out, "Info.Files:\n")
			for _, file := range meta.Info.Files {
				fmt.Fprintf(out, " - %s\n", file.Path)
			}
		}
	}
}

func Main() (err error) {
	var dumpJson bool
	flag.BoolVar(&dumpJson, "json", false, "dump json")
	flag.Parse()
	
	var objs []metaInfoData

	for _, arg := range flag.Args() {
		meta, err := metainfo.LoadFromFile(arg)
		if err != nil {
			return fmt.Errorf("failed to load metainfo: %w", err)
		}
		info, err := meta.UnmarshalInfo()
		if err != nil {
			return fmt.Errorf("failed to unmarshal info: %w", err)
		}
		obj := metaInfoData{
			AnnounceList: meta.AnnounceList,
			CreationDate: meta.CreationDate,
			Comment:      meta.Comment,
			CreatedBy:    meta.CreatedBy,
			Encoding:     meta.Encoding,
			UrlList:      meta.UrlList,
		}
		for _, node := range meta.Nodes {
			obj.Nodes = append(obj.Nodes, string(node))
		}
		obj.Info = infoData{
			Name:    info.Name,
			Length:  info.Length,
			Private: info.Private,
			Source:  info.Source,
		}
		for _, file := range info.Files {
			obj.Info.Files = append(obj.Info.Files, fileInfoData{
				Length: file.Length,
				Path:   filepath.Join(file.Path...),
			})
		}
		objs = append(objs, obj)
	}

	if dumpJson {
		DumpJson(os.Stdout, objs)
	} else {
		DumpReadable(os.Stdout, objs)
	}

	return nil
}

func main() {
	if err := Main(); err != nil {
		log.Printf("error: %+v", err)
		os.Exit(255)
	}
}
